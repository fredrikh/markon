from setuptools import setup
import pathlib

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(name='markon',
      version='1.0.0',
      description='Simple CTMC solver for Python',
      long_description=long_description,
      long_description_content_type='text/markdown',
      url='http://gitlab.com/fredrikh/markon',
      author='Fredrik Bakkevig Haugli',
      author_email='fredrik.haugli@gmail.com',
      license='MIT',
      classifiers=[
           'License :: OSI Approved :: MIT License',
           'Programming Language :: Python :: 3'
      ],
      py_modules=['markon'],
      install_requires=['sympy'],
      project_urls={
          'Bug Reports': 'https://gitlab.com/fredrikh/markon/-/issues',
          'Source': 'https://gitlab.com/fredrikh/markon/'})
