from markon import solve_ctmc
import sympy


def test_1():
    transitions = {}

    transitions[('A', 'B')] = 5
    transitions[('B', 'C')] = 3
    transitions[('C', 'A')] = 1
    transitions[('A', 'C')] = 2
    transitions[('C', 'B')] = 3

    p = solve_ctmc(transitions)

    assert p['A'] == sympy.Rational(3, 50)
    assert p['B'] == sympy.Rational(13, 25)
    assert p['C'] == sympy.Rational(21, 50)
    assert float(p['A']) == 3/50
    assert float(p['B']) == 13/25
    assert float(p['C']) == 21/50


def test_2():
    t = {('A', 'B'): 2,
         ('B', 'A'): 1,
         ('A', 'C'): 1,
         ('C', 'A'): 1,
         ('B', 'C'): 4,
         ('C', 'B'): 8}

    p = solve_ctmc(t)

    assert p['A'] == sympy.Rational(1, 4)
    assert p['B'] == sympy.Rational(1, 2)
    assert p['C'] == sympy.Rational(1, 4)


def check_solution(solved_p, correct_p):
    assert sympy.simplify(solved_p - correct_p) == 0


def test_two_state_comp():
    λ, μ = sympy.symbols('λ μ')
    t = {('up', 'down'): λ,
         ('down', 'up'): μ}

    p = solve_ctmc(t)

    check_solution(p['up'], μ/(λ+μ))
    check_solution(p['down'], λ/(λ+μ))


def test_two_comp_independent_rep():
    """Example 9.4 part 1 from Trivedi & Bobbio"""
    λ, μ = sympy.symbols('λ μ')
    t = {('2', '1'): 2*λ,
         ('1', '0'): λ,
         ('0', '1'): 2*μ,
         ('1', '2'): μ}

    p = solve_ctmc(t)

    check_solution(p['0'], (λ/(λ+μ))**2)
    check_solution(p['1'], 2*λ*μ/((λ+μ)**2))
    check_solution(p['2'], (μ**2/(λ+μ)**2))


def test_two_comp_dependent_rep():
    """Example 9.4 part 2 from Trivedi & Bobbio"""
    λ, μ = sympy.symbols('λ μ')
    t = {('2', '1'): 2*λ,
         ('1', '0'): λ,
         ('0', '1'): μ,
         ('1', '2'): μ}

    p = solve_ctmc(t)

    check_solution(p['0'], (2*λ**2)/(2*λ**2+2*λ*μ+μ**2))
    check_solution(p['1'], (2*λ*μ)/(2*λ**2+2*λ*μ+μ**2))
    check_solution(p['2'], (μ**2)/(2*λ**2+2*λ*μ+μ**2))
